import sys
import os
from datetime import datetime
import pymysql
import pytz

pymysql.install_as_MySQLdb()
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'jti@dnq_acjgut8=2f%^6l*emx1p2&lg3^5)p1t)a9hx61vj2*'
DEBUG = True
ALLOWED_HOSTS = []
AUTH_USER_MODEL = 'usuarios.Usuario'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
DEFAULT_FROM_EMAIL = "con.delantal.test@gmail.com"
SERVER_EMAIL = "con.delantal.test@gmail.com"
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'con.delantal.test@gmail.com'
EMAIL_HOST_PASSWORD = 'condelantal040'

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)
LOCAL_APPS = (
    'apps.NanozISTH',
    'apps.usuarios',
    'apps.documentos',
    'apps.metadatos',
    'apps.index',
    'apps.ofertas',
)
LOGIN_URL = 'http://162.243.67.241/login/'
THIRD_PARTY_APPS = (
    # 'social.apps.django_app.default',
    'suit',
    # 'djrill',
    # 'django_user_agents',
    'dateutil',
    # 'django_filters',
    # 'django_genericfilters',
    # 'watson',
    # 'embed_video',
    # 'mathfilters',
    # 'snowpenguin.django.recaptcha2',
    # 'material',
    # 'material.admin',
    # 'material.frontend',
    # 'easy_pjax',
    # 'redactor',
    # 'pdfkit',
    # 'fedex',
)
INSTALLED_APPS = THIRD_PARTY_APPS + DJANGO_APPS + LOCAL_APPS
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'NanozITSH.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'NanozITSH.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
# '''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'es-MX'

DEFAULT_CHARSET = 'utf-8'
FILE_CHARSET = 'utf-8'

TIME_ZONE = 'America/Mexico_City'

USE_I18N = True

USE_L10N = True

USE_TZ = True
NOMBRE_LOG = "local"
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_ROOT = '/static/'
STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)


def formato_fecha(fecha):
    hoy = datetime(fecha.year, fecha.month, fecha.day, 12, 0, 0, 0, pytz.UTC)
    return hoy.strftime('%Y-%m-%d')


'''
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,

    'formatters': {
        'completo': {
            'format': "[%(asctime)s] %(levelname)s - [%(pathname)s, %(lineno)s], %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },

        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },

    'handlers': {
        'archivo': {
            'class': 'logging.FileHandler',
            'filename': "logs/" + formato_fecha(datetime.today()) + ".log",
            'formatter': 'completo'
        },
        'db': {
            'class': 'apps.index.models.NanozLog',
        },

        'consola': {
            'class': 'logging.StreamHandler',
            'stream': sys.stdout,
            'formatter': 'completo'
        }
    },

    'loggers': {
        'produccion': {
            'handlers': ['db', 'archivo'],
            'propagate': True,
            'level': 'DEBUG',
        },

        'staging': {
            'handlers': ['db', 'archivo', 'consola'],
            'propagate': True,
            'level': 'DEBUG',
        },

        'local': {
            'handlers': ['consola'],
            'propagate': True,
            'level': 'DEBUG'
        }
    }
}
#'''
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
