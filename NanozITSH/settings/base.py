import os
from datetime import datetime

import pytz
from unipath import Path

NOMBRE_LOG = "local"
def formato_fecha(fecha):
    hoy = datetime(fecha.year, fecha.month, fecha.day, 12, 0, 0, 0, pytz.UTC)
    return hoy.strftime('%Y-%m-%d')
BASE_DIR = Path(__file__).ancestor(3)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'jti@dnq_acjgut8=2f%^6l*emx1p2&lg3^5)p1t)a9hx61vj2*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# **************    CONFIGURACION DE CORREOS
ADMINS = (('Ricardo_Vera', 'isc4.tec@gmail.com'),)

MANAGERS = (("Ricardo_Vera", "isc4.tec@gmail.com"),)
# """
EMAIL_BACKEND = 'django.core.mail.backends.base.EmailBackend'
DEFAULT_FROM_EMAIL = "con.delantal.test@gmail.com"
SERVER_EMAIL = "con.delantal.test@gmail.com"
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'con.delantal.test@gmail.com'
EMAIL_HOST_PASSWORD = 'condelantal040'
"""
SENDGRID_API_KEY = "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"
EMAIL_BACKEND = 'sgbackend.SendGridBackend'
"""
# Application definition

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

SITE_ID = 1

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
]

LOCAL_APPS = (
    'apps.NanozISTH',
    'apps.usuarios',
    'apps.documentos',
    'apps.metadatos',
    'apps.index',
    'apps.ofertas',

)

THIRD_PARTY_APPS = (
    'suit',
    'django_user_agents',
    'dateutil',
    'pymysql',

)
'''
CART_PRODUCT_MODEL = 'apps.servicios.models.Platillo'
CART_SESSION_KEY = "NanozCart"
CART_TEMPLATE_TAG_NAME = "carrito"

GEOPOSITION_GOOGLE_MAPS_API_KEY = 'AIzaSyC6UBtxDBeoxSoenu2vBIpgjcuRSk-O29Q'
GOOGLE_MAPS_API_KEY = 'AIzaSyC6UBtxDBeoxSoenu2vBIpgjcuRSk-O29Q'
'''
INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS + THIRD_PARTY_APPS

ROOT_URLCONF = 'NanozITSH.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                # 'material.frontend.context_processors.modules',
                'django.core.context_processors.static',
            ],
        },
    },
]
WSGI_APPLICATION = 'NanozITSH.wsgi.application'
# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators
'''
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
'''
# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

USE_I18N = True

USE_L10N = True

USE_TZ = True
AUTH_USER_MODEL = 'usuarios.Usuario'
LANGUAGE_CODE = 'es-MX'

TIME_ZONE = 'America/Mexico_City'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

DEFAULT_CHARSET = 'utf-8'
FILE_CHARSET = 'utf-8'

# media files
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# static files
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'