from .base import *
import pymysql

pymysql.install_as_MySQLdb()
# '''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'nanozDB.sqlite'),
    }
}
'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'nanozDB',
        'HOST': 'localhost',
        'USER': 'root',
        'PASSWORD': '123456',
        'PORT': '3306',
    }
}
# '''

CONEKTA_PRIVATE = ""
CONEKTA_PUBLIC = ""
