from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings

urlpatterns = [
    # Examples:
    # url(r'^$', 'NanozITSH.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^noSoy1admin/', include(admin.site.urls)),
    url(r'^', include('apps.index.urls', namespace='index_app')),
    url(r'^login/$', 'apps.usuarios.views.loginUsuario', name='login'),
    url(r'^registrar_oferta/$', 'apps.ofertas.views.nuevaOferta', name='OfertaNueva'),
    url(r'^salir/$', 'apps.usuarios.views.salir', name='salir'),
    url(r'^registro/$', 'apps.usuarios.views.registro', name='registro'),
    url(r'^acercaDe/$', 'apps.usuarios.views.acercaDe', name='acercaDe'),
    url(r'^egresados/$', 'apps.ofertas.views.Egresados', name='egresados'),
    url(r'^Oferta/$', 'apps.ofertas.views.detalleOferta', name='oferta'),
    #url(r'^test/$', 'apps.usuarios.views.test1', name='test'),
    url(r'^test/$', 'apps.usuarios.views.testDinamico', name='test2'),
    url(r'^terminos-y-condiciones/$', 'apps.usuarios.views.UsosYCondiciones', name='UsosYCondiciones'),

]
urlpatterns += patterns('', (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
    'document_root': settings.MEDIA_ROOT}))
