from django.contrib import admin
from apps.ofertas.models import OfertaEmpleo

# Register your models here.

@admin.register(OfertaEmpleo)
class AdminOfertaEmpleo(admin.ModelAdmin):
    list_display = ("nombre", "empresa", "fecha", "activo",)
    list_editable = ("activo",)
    list_filter = ("activo", "empresa",)
    search_fields = ("nombre", "empresa",)
