from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

# Create your views here.
from apps.ofertas.forms import OfertaEmpleoForm
from apps.ofertas.models import OfertaEmpleo


# @login_required
def Egresados(request):
    user = request.user
    if not user.is_authenticated():
        return redirect(reverse('login'))
    if user.status == False:
        return redirect(reverse('test'))
    carrera = user.carreraEgreso
    ofertasEmpleos = OfertaEmpleo.objects.filter(activo=True, carrera=carrera).order_by("fecha")
    if not ofertasEmpleos.exists():
        mensaje = "Sin Ofertas de empleo para la carrera de " + carrera.nombre
    return render(request, "index/egresados.html", {
        'ofertasEmpleos': ofertasEmpleos,
        "mensaje": mensaje
    })


# @login_required
def detalleOferta(request):
    user = request.user
    if not user.is_authenticated():
        return redirect(reverse('login'))
    if user.status == False:
        return redirect(reverse('test'))
    if request.method == "POST":
        if "id" in request.POST:
            try:
                oferta = OfertaEmpleo.objects.filter(id=request.POST["id"])
                if oferta.exists():
                    return render(request, "index/detalleOfertaEmpleo.html", {
                        'ofertaEmpleo': oferta[0]
                    })
            except Exception as e:
                pass
    ofertasEmpleos = OfertaEmpleo.objects.filter(activo=True).order_by("fecha")
    return render(request, "index/egresados.html", {
        'ofertasEmpleos': ofertasEmpleos,
    })


def nuevaOferta(request):
    if request.method == "POST":
        FormOferta = OfertaEmpleoForm(request.POST, request.FILES)
        if FormOferta.is_valid():
            FormOferta.save()
            return redirect(reverse('index_app:index'))
        else:
            print(FormOferta.errors())
    FormOferta = OfertaEmpleoForm()
    return render(request, "ofertas/registro_nueva_oferta.html", {
        "form_oferta": FormOferta
    })
