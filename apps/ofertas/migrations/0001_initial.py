# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.ofertas.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='OfertaEmpleo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=500)),
                ('fecha', models.DateField(auto_now_add=True)),
                ('descripcion', models.TextField()),
                ('ubicacion', models.CharField(max_length=300)),
                ('requisitos', models.TextField()),
                ('contacto', models.TextField()),
                ('empresa', models.CharField(max_length=100)),
                ('imagen', models.ImageField(upload_to=apps.ofertas.models.uploadImagen)),
                ('telefono', models.CharField(max_length=20)),
                ('activo', models.BooleanField(default=False)),
            ],
        ),
    ]
