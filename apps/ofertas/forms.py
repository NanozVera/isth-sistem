# -*- encoding: utf-8 -*-

from django import forms

from apps.ofertas.models import OfertaEmpleo
from apps.usuarios.models import Carrera


class OfertaEmpleoForm(forms.ModelForm):
    class Meta:
        model = OfertaEmpleo
        fields = (
            'nombre',
            'descripcion',
            'ubicacion',
            'requisitos',
            'contacto',
            'empresa',
            'telefono',
            'imagen',
            'carreras',
        )

    widgets = {
        'contacto': forms.TextInput(attrs={
            'required': 'true',
            'type': 'email ',
            'class': 'required',
            'rows': '10'
        }),
        'nombre': forms.TextInput(attrs={
            'required': 'true',
            'class': 'form-control required',
        }),
        'ubicacion': forms.TextInput(attrs={
            'required': 'true',
            'class': 'form-control required',
        }),
        'requisitos': forms.TextInput(attrs={
            'required': 'true',
            'class': 'form-control required',
            'rows': '10'
        }),
        'descripcion': forms.TextInput(attrs={
            'required': 'true',
            'class': 'form-control required',
            'rows': '10'
        }),
        'empresa': forms.TextInput(attrs={
            'required': 'true',
            'class': 'form-control required',
        }),
        'telefono': forms.TextInput(attrs={
            'required': 'true',
            'class': 'form-control'
        }),
        'imagen': forms.FileInput(attrs={
            'required': 'true',
            'type': 'file',
            'accept': 'image/*',
            'class': 'form-control required',

        }),
        'carreras': forms.ModelChoiceField(
                queryset=Carrera.objects.all(),
                empty_label=None
        )
    }
    labels = {
        'nombre': "Nombre",
        'descripcion': "Descripcion",
        'ubicacion': "Ubicacion",
        'requisitos': "Requisitos",
        'contacto': "Contacto",
        'empresa': "Empresa",
        'telefono': "Telefono",
        'imagen': "Imagen",

    }

    def __init__(self, *args, **kwargs):
        super(OfertaEmpleoForm, self).__init__(*args, **kwargs)
        self.fields["contacto"].widget.attrs['required'] = "required"
        self.fields["contacto"].required = True
        self.fields["nombre"].widget.attrs['required'] = "required"
        self.fields["nombre"].required = True
        self.fields["ubicacion"].widget.attrs['required'] = "required"
        self.fields["ubicacion"].required = True
        self.fields["requisitos"].widget.attrs['required'] = "required"
        self.fields["requisitos"].required = True
        self.fields["descripcion"].widget.attrs['required'] = "required"
        self.fields["descripcion"].required = True
        self.fields["empresa"].widget.attrs['required'] = "required"
        self.fields["empresa"].required = True
        self.fields["telefono"].widget.attrs['required'] = "required"
        self.fields["telefono"].required = True
        self.fields["imagen"].widget.attrs['required'] = "required"
        self.fields["imagen"].required = True
