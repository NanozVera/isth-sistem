from django.db import models

# Create your models here.
from apps.usuarios.models import Carrera


def uploadImagen(instance, filename):
    return "{file}".format(file=filename)


class OfertaEmpleo(models.Model):
    nombre = models.CharField(max_length=500)
    fecha = models.DateField(auto_now_add=True)
    descripcion = models.TextField()
    ubicacion = models.CharField(max_length=300)
    requisitos = models.TextField()
    contacto = models.EmailField(max_length=100)
    empresa = models.CharField(max_length=100)
    imagen = models.ImageField(upload_to=uploadImagen)
    telefono = models.CharField(max_length=20)
    activo = models.BooleanField(default=False)
    carreras = models.ManyToManyField(Carrera)

    @property
    def dameImagen(self):
        try:
            return self.imagen._get_url
        except:
            return ""
