from django.contrib import admin

# Register your models here.
from apps.documentos.models import FormularioResidencias, FormularioServicio, Pregunta, Respuesta, Cuestionario

'''
admin.site.register(FormularioResidencias)
admin.site.register(FormularioServicio)
admin.site.register(OfertaEmpleo)
'''


@admin.register(FormularioResidencias)
class AdminFormularioResidencias(admin.ModelAdmin):
    pass


@admin.register(FormularioServicio)
class AdminFormularioServicio(admin.ModelAdmin):
    pass


@admin.register(Pregunta)
class AdminPregunta(admin.ModelAdmin):
    pass


@admin.register(Cuestionario)
class AdminCuestionario(admin.ModelAdmin):
    pass


@admin.register(Respuesta)
class AdminRespuesta(admin.ModelAdmin):
    pass
