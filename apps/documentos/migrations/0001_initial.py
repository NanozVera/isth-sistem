# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FormularioResidencias',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('fechaEgreso', models.DateField(blank=True, null=True)),
                ('residencias', models.CharField(max_length=100, default='')),
                ('domicilioActual', models.TextField(max_length=1000)),
                ('telefono', models.CharField(max_length=20)),
                ('empleoActual', models.TextField(max_length=1000)),
                ('enfoqueEmpleo', models.BooleanField(default=False)),
                ('pregunta1', models.BooleanField(default=False)),
                ('pregunta2', models.TextField(max_length=1000)),
                ('pregunta3', models.BooleanField(default=False)),
                ('pregunta4', models.TextField(max_length=1000)),
                ('pregunta5', models.TextField(max_length=1000)),
                ('pregunta6', models.BooleanField(default=False)),
                ('pregunta7', models.TextField(max_length=1000)),
                ('pregunta8', models.BooleanField(default=False)),
                ('pregunta9', models.TextField(max_length=1000)),
                ('pregunta10', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='FormularioServicio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('fechaInicio', models.DateTimeField(blank=True, null=True)),
                ('fechaFin', models.DateTimeField(blank=True, null=True)),
                ('lugar', models.TextField(max_length=1000)),
                ('domicilio', models.TextField(max_length=1000)),
                ('proyecto', models.TextField(max_length=1000)),
                ('nombreEncargado', models.TextField(max_length=1000)),
                ('telefono', models.CharField(max_length=20)),
            ],
        ),
    ]
