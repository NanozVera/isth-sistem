from django.db import models
# Create your models here.
from apps.usuarios.models import Usuario, Egresado

tipos_de_respuestas = ((1, "Texto"), (2, "Si No"), (3, "Imagen"), (4, "Numero"))
respuestas = ["", "Texto", "Si No", "Imagen", "Numero"]


class FormularioResidencias(models.Model):
    usuario = models.ForeignKey(Usuario)
    fechaEgreso = models.DateField(blank=True, null=True)
    residencias = models.CharField(max_length=100, default="")
    domicilioActual = models.TextField(max_length=1000)
    telefono = models.CharField(max_length=20)
    empleoActual = models.TextField(max_length=1000)
    enfoqueEmpleo = models.BooleanField(default=False)
    pregunta1 = models.BooleanField(default=False)
    pregunta2 = models.TextField(max_length=1000)
    pregunta3 = models.BooleanField(default=False)
    pregunta4 = models.TextField(max_length=1000)
    pregunta5 = models.TextField(max_length=1000)
    pregunta6 = models.BooleanField(default=False)
    pregunta7 = models.TextField(max_length=1000)
    pregunta8 = models.BooleanField(default=False)
    pregunta9 = models.TextField(max_length=1000)
    pregunta10 = models.BooleanField(default=False)


class FormularioServicio(models.Model):
    usuario = models.ForeignKey(Usuario)
    fechaInicio = models.DateTimeField(blank=True, null=True)
    fechaFin = models.DateTimeField(blank=True, null=True)
    lugar = models.TextField(max_length=1000)
    domicilio = models.TextField(max_length=1000)
    proyecto = models.TextField(max_length=1000)
    nombreEncargado = models.TextField(max_length=1000)
    telefono = models.CharField(max_length=20)


class Pregunta(models.Model):
    texto = models.CharField(max_length=500)
    tipo = models.IntegerField(choices=tipos_de_respuestas, default=0)

    def __unicode__(self):
        return str(self.texto) + "/" + str(respuestas[self.tipo])

    def __str__(self):
        return str(self.texto) + "/" + str(respuestas[self.tipo])


class Cuestionario(models.Model):
    listado = models.ManyToManyField(Pregunta, verbose_name="Lista de Preguntas")
    fecha = models.DateField(auto_now_add=True)
    descripcion = models.TextField(max_length=300, blank=True, null=True)
    activo = models.BooleanField(default=False)

    def __str__(self):
        return self.descripcion

    def __unicode__(self):
        return self.descripcion


class Respuesta(models.Model):
    usuario = models.ForeignKey(Usuario)
    cuestionario = models.IntegerField(default=0)
    fecha = models.DateField(auto_now_add=True)

    pregunta = models.ForeignKey(Pregunta)
    texto = models.CharField(max_length=10000)
    si_no = models.IntegerField(verbose_name="SI/NO", default=-1)
    imagen = models.FileField()
    valor = models.IntegerField(default=-1)

    def __str__(self):
        return str(self.cuestionario) + "/" + str(self.usuario) + "/" + str(self.fecha)

    def __unicode__(self):
        return str(self.cuestionario) + "/" + str(self.usuario) + "/" + str(self.fecha)


"""
class SeRealizo(models.Model):
    egresado = models.ForeignKey(Usuario)
    encuesta = models.ForeignKey(Encuesta)


class Encuesta(models.Model):
    tipos = (("Servicio", "Servicio"), ("Residencias", "Residencias"), ("Egresado", "Egresado"))
    tipo = models.CharField(max_length=20, choices=tipos)



class ContenidoEncuesta(models.Model):
    encuesta = models.ForeignKey(Encuesta)
    pregunta = models.ForeignKey(Pregunta)

class Pregunta(models.Model):
    pregunta = models.CharField(max_length=300)
    opcion = models.ForeignKey(Opcion)


class Opcion(models.Model):
    tipo = models.CharField(max_length=200)


class Respuesta(models.Model):
    egresado = models.ForeignKey(Egresado)
    pregunta = models.ForeignKey(ContenidoEncuesta)
    respuesta = models.CharField(max_length=1000)
"""
