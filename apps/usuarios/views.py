from datetime import date

from django.contrib.auth import login, logout
from django.shortcuts import render, redirect

from apps.documentos.models import FormularioResidencias, Cuestionario, Respuesta
from apps.usuarios.forms import FormCrearUsuario
from apps.usuarios.models import Carrera, Usuario
from django.core.urlresolvers import reverse


def CreacionUsuarios(request):
    formCreacion = FormCrearUsuario()
    return render(request, 'index/login.html', {
        'formCreacion': formCreacion
    })


def listaAnos():
    anoFin = (date.today().year) - 4
    anoInicio = 2001
    lista = []
    while (anoInicio < anoFin):
        otroAno = anoInicio + 4
        fecha = str(anoInicio) + " - " + str(otroAno)
        lista.append(fecha)
        anoInicio += 1
    return lista


def salir(request):
    logout(request)
    return render(request, 'index/index.html')


def loginUsuario(request):
    if request.method == "POST":
        if "username" in request.POST and "password" in request.POST:
            users = Usuario.objects.filter(email=request.POST["username"])
            if users.exists():
                password = users[0].check_password(str(request.POST["password"]))
                if password:
                    user = users[0]
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    login(request, user)
                    return redirect(reverse('egresados'))
            return render(request, "index/login.html", {'error': "Usuario o contraseña mal"})
    return render(request, "index/login.html")


def registro(request):
    error = None
    if request.user.is_authenticated():
        return redirect(reverse('egresados'))
    if request.method == "POST":
        try:
            matricula = str(request.POST['matricula']).strip()
            indice = int(matricula[:2])
            zeta = matricula[3:4].lower()
            anio = date.today().year - 2000
            if indice < anio and zeta == "z":
                carrera = Carrera.objects.get(nombre=request.POST["carrera"])
                user = Usuario.objects.create(
                        username=str(request.POST["email"]).strip(),
                        nombre=str(request.POST["name"]).strip(),
                        apellidos=(str(request.POST["apellidop"]) + " " + str(request.POST["apellidom"])).strip(),
                        email=str(request.POST["email"]).strip(),
                        matricula=matricula,
                        carreraEgreso=carrera.id,
                        sexo=request.POST["sexo"],
                        password=str(request.POST["Pass1"])
                )
                user.set_password(request.POST["Pass1"])
                user.save()
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                return redirect(reverse('egresados'))
            else:
                error = "matricula no valida"
        except:
            error = "matricula no valida"
    carreras = Carrera.objects.all()
    return render(request, 'index/loginV2.html', {
        'carreras': carreras,
        'error': error,
        'años': listaAnos()
    })


def acercaDe(request):
    return render(request, 'index/acercaDe.html')


def UsosYCondiciones(request):
    return render(request, 'UsusYCondiciones.html')


def test1(request):
    user = request.user
    error = None
    if not user.is_authenticated():
        return redirect(reverse('login'))
    if request.method == "POST":
        try:
            user = request.user
            FormularioResidencias.objects.create(
                    usuario=user,
                    residencias=request.POST['RESIDENCIAS'],
                    domicilioActual=request.POST['DOMICILIO'],
                    telefono=request.POST['TELEFONO'],
                    empleoActual=request.POST['EMPLEO_ACTUAL'],
                    enfoqueEmpleo=int(request.POST['ENFOQUE']),
                    pregunta1=int(request.POST['NIVEL']),
                    pregunta2=request.POST['NECESIDADES'],
                    pregunta3=int(request.POST['ZONA']),
                    pregunta4=request.POST['NO_ENCONTRADO'],
                    pregunta5=request.POST['DIFICULTADES'],
                    pregunta6=int(request.POST['OP_POSGRADO']),
                    pregunta7=request.POST['PORQUE'],
                    pregunta8=int(request.POST['FORO']),
                    pregunta9=request.POST['SUGERENCIA'],
                    pregunta10=int(request.POST['FORMACION']),
            )
            user.status = True
            user.save()
            return redirect(reverse('egresados'))
        except Exception as e:
            error = e
    carreras = Carrera.objects.all()
    return render(request, 'usuarios/test1.html', {
        "carreras": carreras,
        "error": error,
        'anios': listaAnos()
    })


def testDinamico(request):
    print(request.POST)
    print(request.GET)
    print(request.FILES)
    user = request.user
    error = None
    cuestionario = Cuestionario.objects.filter(activo=True)[0]
    preguntas = cuestionario.listado.all()
    if not user.is_authenticated():
        return redirect(reverse('login'))
    if request.method == "POST":
        try:
            user.status = True
            user.save()
            for pregunta in preguntas:
                respuesta = Respuesta.objects.create(
                        usuario=user,
                        cuestionario=cuestionario.id,
                        pregunta=pregunta,
                )
                if pregunta.tipo == 1:
                    respuesta.texto = request.POST[str(pregunta.id)]
                if pregunta.tipo == 2:
                    value = request.POST[str(pregunta.id)]
                    if value == "1" or value == 1:
                        respuesta.si_no = 1
                    else:
                        respuesta.si_no = 2
                if pregunta.tipo == 3:
                    respuesta.imagen = request.FILES[str(pregunta.id)]
                if pregunta.tipo == 4:
                    respuesta.textos = request.POST[str(pregunta.id)]
                pregunta.save()
        except Exception as e:
            print(e.args)
        return redirect(reverse('egresados'))
    carreras = Carrera.objects.all()
    return render(request, 'usuarios/testDinamico.html', {
        "carreras": carreras,
        "preguntas": preguntas,
        "error": error,
        'anios': listaAnos()
    })
