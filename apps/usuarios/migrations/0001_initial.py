# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(verbose_name='last login', blank=True, null=True)),
                ('is_superuser', models.BooleanField(verbose_name='superuser status', help_text='Designates that this user has all permissions without explicitly assigning them.', default=False)),
                ('username', models.CharField(unique=True, max_length=40)),
                ('nombre', models.CharField(blank=True, max_length=40)),
                ('apellidos', models.CharField(blank=True, max_length=40)),
                ('matricula', models.CharField(blank=True, max_length=10)),
                ('fecha_registro', models.DateTimeField(auto_now_add=True)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('status', models.BooleanField(default=False)),
                ('carreraEgreso', models.IntegerField(default=0)),
                ('anoEgreso', models.CharField(choices=[('2001 - 2005', '2001 - 2005'), ('2002 - 2006', '2002 - 2006'), ('2003 - 2007', '2003 - 2007'), ('2004 - 2008', '2004 - 2008'), ('2005 - 2009', '2005 - 2009'), ('2006 - 2010', '2006 - 2010'), ('2007 - 2011', '2007 - 2011'), ('2008 - 2012', '2008 - 2012'), ('2009 - 2013', '2009 - 2013'), ('2010 - 2014', '2010 - 2014'), ('2011 - 2015', '2011 - 2015')], max_length=20)),
                ('sexo', models.CharField(max_length=10)),
                ('is_active', models.BooleanField(default=True)),
                ('is_staff', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(verbose_name='groups', related_name='user_set', blank=True, related_query_name='user', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', to='auth.Group')),
                ('user_permissions', models.ManyToManyField(verbose_name='user permissions', related_name='user_set', blank=True, related_query_name='user', help_text='Specific permissions for this user.', to='auth.Permission')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Carrera',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Creditos',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('cantidad', models.IntegerField(default=0)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Direccion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('calle', models.CharField(max_length=40)),
                ('colonia', models.CharField(max_length=40)),
                ('codigo_postañ', models.CharField(max_length=5)),
            ],
        ),
        migrations.CreateModel(
            name='Egresado',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=100, default='')),
                ('apellidos', models.CharField(max_length=100, default='')),
                ('num_control', models.CharField(max_length=100, default='')),
                ('telefono', models.CharField(blank=True, null=True, max_length=20)),
                ('fecha_nacimiento', models.DateField(blank=True, null=True)),
                ('sexo', models.CharField(choices=[('H', 'H'), ('M', 'M')], max_length=1)),
            ],
        ),
        migrations.CreateModel(
            name='Modalidad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Pertenece',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('carrera', models.ForeignKey(to='usuarios.Carrera')),
                ('egresado', models.ForeignKey(to='usuarios.Egresado')),
                ('modalidad', models.ForeignKey(to='usuarios.Modalidad')),
            ],
        ),
    ]
