from django import forms
from django.utils.translation import ugettext_lazy as _

from apps.usuarios.models import Usuario


class FormCrearUsuario(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('username', 'email', 'password')
        widgets = {
            'username': forms.TextInput(attrs={
                #'class': 'form-control',
                'required': 'true',
            }),
            'email': forms.EmailInput(attrs={
                'type': 'email',
                #'class': 'form-control',
                'required': 'true',
            }),
            'password': forms.TextInput(attrs={
                'type': 'password',
                #'class': 'form-control psw',
                'required': 'true',
            }),
        }
        labels = {
            'username': _("Nombre de usuario"),
            'email': _("Correo Electronico"),
            'password': _("Contraseña"),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        usuario = True
        correo = True
        if len(cleaned_data.get("username")) < 4:
            self._errors['username'] = self.error_class(["El nombre de usuario debe contener mínimo 4 caracteres"])
            usuario = False
        if len(cleaned_data.get("password")) < 6:
            self._errors['password'] = self.error_class(["La contraseña debe contener mínimo 6 caracteres"])
        if Usuario.objects.filter(username=cleaned_data.get("username").lower().strip().replace(' ', '_')):
            # print("El nombre de usuario ya esta en uso")
            self._errors['username'] = self.error_class(["El nombre de usuario ya esta en uso"])
            usuario = False

        if usuario and correo:
            return cleaned_data

    def __init__(self, *args, **kwargs):
        super(FormCrearUsuario, self).__init__(*args, **kwargs)
        self.fields['username'].required = True
        self.fields['email'].required = True
        self.fields['password'].required = True
