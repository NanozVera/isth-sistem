from django.contrib import admin

from apps.documentos.models import FormularioResidencias, FormularioServicio
from apps.usuarios.models import Usuario, Carrera, Creditos


class servicioInLine(admin.TabularInline):
    model = FormularioServicio
    extra = 0
    fields = (
        'fechaInicio',
        'fechaFin',
        'lugar',
        'domicilio',
        'proyecto',
        'nombreEncargado',
        'telefono',
    )
    readonly_fields = (
        'fechaInicio',
        'fechaFin',
        'lugar',
        'domicilio',
        'proyecto',
        'nombreEncargado',
        'telefono',
    )
    can_delete = False

    def has_add_permission(self, request):
        return False


class residenciasInLine(admin.TabularInline):
    model = FormularioResidencias
    extra = 0
    fields = (
        'fechaEgreso',
        'residencias',
        'domicilioActual',
        'telefono',
        'empleoActual',
        'enfoqueEmpleo',
        'pregunta1',
        'pregunta2',
        'pregunta3',
        'pregunta4',
        'pregunta5',
        'pregunta6',
        'pregunta7',
        'pregunta8',
        'pregunta9',
        'pregunta10',
    )
    readonly_fields = (
        'fechaEgreso',
        'residencias',
        'domicilioActual',
        'telefono',
        'empleoActual',
        'enfoqueEmpleo',
        'pregunta1',
        'pregunta2',
        'pregunta3',
        'pregunta4',
        'pregunta5',
        'pregunta6',
        'pregunta7',
        'pregunta8',
        'pregunta9',
        'pregunta10',
    )
    can_delete = False

    def has_add_permission(self, request):
        return False


@admin.register(Carrera)
class AdminCarrera(admin.ModelAdmin):
    pass


@admin.register(Usuario)
class AdminUsuario(admin.ModelAdmin):
    list_display = (
        'id',
        'username',
        'email',
        'is_active',
        'is_staff',
    )
    list_display_links = ('username',)
    fieldsets = [
        ('Datos Personales', {'fields': ['username', 'nombre', 'apellidos', 'carreraEgreso']}),
        ('Otros Datos', {'fields': ['email', 'matricula', 'anoEgreso']}),
        ('Super poderes', {'fields': ['is_superuser', 'groups', 'is_staff', 'is_active', ]}),
    ]
    search_fields = ('email', 'username',)

    inlines = [
        servicioInLine,
        residenciasInLine
    ]


@admin.register(Creditos)
class AdminCreditos(admin.ModelAdmin):
    pass
