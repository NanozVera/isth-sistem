from datetime import date
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models


class UserManager(BaseUserManager, models.Manager):
    def _create_user(self, username, email, password, is_staff, is_superuser, is_active=False, **extra_fields):
        email = self.normalize_email(email)

        user = self.model(username=username, email=email, is_active=is_active,
                          is_staff=is_staff, is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email, password=None, is_active=True, **extra_fields):
        # print('creando usuario')
        return self._create_user(username, email, password, False, False, is_active=True, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, True, True, is_active=True, **extra_fields)


def listaAnos():
    anoFin = (date.today().year) - 4
    anoInicio = 2001
    lista = []
    while (anoInicio < anoFin):
        otroAno = anoInicio + 4
        fecha = str(anoInicio) + " - " + str(otroAno)
        lista.append((fecha, fecha))
        anoInicio += 1
    return lista


class Carrera(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return self.nombre


class Modalidad(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return self.nombre


class Usuario(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=40, unique=True)
    nombre = models.CharField(max_length=40, blank=True)
    apellidos = models.CharField(max_length=40, blank=True)
    matricula = models.CharField(max_length=10, blank=True)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    email = models.EmailField(unique=True)
    status = models.BooleanField(default=False)  # para validar al usuario
    carreraEgreso = models.IntegerField(default=0)
    anoEgreso = models.CharField(max_length=20, choices=listaAnos())
    objects = UserManager()
    sexo = models.CharField(max_length=10)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    @property
    def get_short_name(self):
        if self.nombre != "":
            return self.nombre + self.matricula
        elif self.username != "":
            return self.username + self.matricula
        else:
            return self.email + self.matricula

    @property
    def get_full_name(self):
        if self.nombre != "":
            return self.nombre + " " + self.apellidos
        elif self.username != "":
            return self.username
        else:
            return str(self.email)

    def __str__(self):
        return self.get_short_name

    def __unicode__(self):
        return self.get_short_name


class Creditos(models.Model):
    usuario = models.ForeignKey(Usuario)
    cantidad = models.IntegerField(default=0)


class Egresado(models.Model):
    nombre = models.CharField(max_length=100, default="")
    apellidos = models.CharField(max_length=100, default="")
    num_control = models.CharField(max_length=100, default="")
    telefono = models.CharField(max_length=20, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    sexos = (("H", "H"), ("M", "M"))
    sexo = models.CharField(max_length=1, choices=sexos)
    # direccion = models.ForeignKey(Direccion, blank=True, null=True)


class Direccion(models.Model):
    calle = models.CharField(max_length=40)
    colonia = models.CharField(max_length=40)
    codigo_postañ = models.CharField(max_length=5)


class Pertenece(models.Model):
    egresado = models.ForeignKey(Egresado)
    carrera = models.ForeignKey(Carrera)
    modalidad = models.ForeignKey(Modalidad)

