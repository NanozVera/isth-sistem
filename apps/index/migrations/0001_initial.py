# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('usuario', models.IntegerField(default=0)),
                ('titulo', models.TextField()),
                ('nivel', models.CharField(max_length=100)),
                ('descripcion', models.TextField()),
                ('ubicacion', models.CharField(max_length=500)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
