from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'apps.index.views.home', name='index'),
    url(r'^test$', 'apps.index.views.test', name='test'),
]
