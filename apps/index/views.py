from django.shortcuts import render

# Create your views here.
from apps.metadatos.funciones import guardarMetadatos


def home(request):
    # METADATO
    guardarMetadatos(request)
    return render(request, 'index/index.html')


def test(request):
    return render(request, 'index/index.html', {})
