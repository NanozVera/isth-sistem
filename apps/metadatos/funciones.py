# -*- encoding: utf-8 -*-

from ipware.ip import get_ip
from ipware.ip import get_real_ip

from apps.index.funciones import logger
from apps.metadatos.models import Metadatos


#   Metodo para Obtener la url con la que llego a la vista actual
def get_referer_view(request, default=''):
    # if the user typed the url directly in the browser's address bar
    referer = request.META.get('HTTP_REFERER')
    if not referer:
        return default
    return referer


def getDispositivo(user_agent):
    if user_agent.is_mobile:  #
        return 'celular'
    elif user_agent.is_tablet:  #
        return 'tablet'
    elif user_agent.is_pc:  #
        return 'pc'
    elif user_agent.is_bot:  #
        return 'bot'
    elif user_agent.is_touch_capable:  #
        return 'dispositivo_touch'
    else:
        return 'desconocido'


def getNavegador(user_agent):
    return user_agent.browser.family


#   sistema operativo
def getOS(user_agent):
    return user_agent.os.family


def getIP(request):
    ip = get_real_ip(request)
    if ip is not None:
        return ip
    else:
        return get_ip(request)


def getAnterior(request):
    return get_referer_view(request)


def guardarMetadatos(request, criterio_busqueda='', id_proyecto=0):
    pagina_actual = request.get_full_path()
    pagina_anterior = get_referer_view(request)
    # construimos la insercion a la base de datos
    id_usuario = 0
    superUsuario = False
    if (request.user.is_authenticated()):
        id_usuario = request.user.id
        superUsuario = request.user.is_superuser

    if not superUsuario:
        try:
            Metadatos.objects.create(ip=getIP(request),
                                     pagina_visitada=pagina_actual,
                                     proyecto=id_proyecto,
                                     sistema_operativo=getOS(request.user_agent),
                                     navegador=getNavegador(request.user_agent),
                                     pagina_procedencia=pagina_anterior,
                                     usuario=id_usuario,
                                     busqueda=criterio_busqueda,
                                     dispositivo=getDispositivo(request.user_agent)
                                     )
        except Exception as e:
            pass
