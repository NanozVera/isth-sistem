# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Metadatos',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('ip', models.CharField(max_length=20)),
                ('usuario', models.IntegerField(default=0)),
                ('dia_hora', models.DateTimeField(auto_now_add=True)),
                ('pagina_visitada', models.URLField()),
                ('proyecto', models.IntegerField(default=0)),
                ('sistema_operativo', models.CharField(max_length=100)),
                ('navegador', models.CharField(max_length=30)),
                ('pagina_procedencia', models.URLField()),
                ('dispositivo', models.CharField(max_length=20)),
                ('busqueda', models.CharField(max_length=50)),
            ],
        ),
    ]
