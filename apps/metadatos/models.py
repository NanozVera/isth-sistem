from django.db import models
from apps.usuarios.models import Usuario


class Metadatos(models.Model):
    ip = models.CharField(max_length=20)
    usuario = models.IntegerField(default=0)
    dia_hora = models.DateTimeField(auto_now_add=True)
    pagina_visitada = models.URLField()
    proyecto = models.IntegerField(default=0)
    sistema_operativo = models.CharField(max_length=100)
    navegador = models.CharField(max_length=30)
    pagina_procedencia = models.URLField()
    dispositivo = models.CharField(max_length=20)
    busqueda = models.CharField(max_length=50)

    def __str__(self):
        if self.usuario != 0:
            try:
                return Usuario.objects.get(id=self.usuario).username
            except:
                return 'anonimo'
        else:
            return 'Anonimo'

    def __unicode__(self):
        if self.usuario != 0:
            try:
                return Usuario.objects.get(id=self.usuario).username
            except:
                return 'anonimo'
        else:
            return 'Anonimo'
    @property
    def username(self):
        if self.usuario != 0:
            try:
                return Usuario.objects.get(id=self.usuario).username
            except:
                return 'anonimo'
        else:
            return 'Anonimo'
